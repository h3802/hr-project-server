from django.shortcuts import redirect, Http404
from django.contrib.auth import get_user_model
from djoser.conf import settings
from djoser.signals import user_registered
from djoser.serializers import UserCreatePasswordRetypeSerializer
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from rest_framework.viewsets import ViewSet
from rest_framework.permissions import AllowAny

from helpers.application_viewsets import ApplicationReadOnlyViewSet, ApplicationViewSet
from .models import AccountRole, UserAccount
from .serializers import UserCustomSerializer, MemberCreateSerializer, CandidateSerializer
from .permissions import ReadOnlyPermission, SupervisorPermission, TechManagerPermission
from worksheets.models import Answer

User = get_user_model()


class ActivationRedirect(APIView):
    def get(self, request, uid, token):
        return redirect(to=f'http://localhost:8080/#/activation/{uid}/{token}')


class MemberListingViewSet(ApplicationReadOnlyViewSet):
    permission_classes = [ReadOnlyPermission]
    serializer_class = UserCustomSerializer

    def get_queryset(self):
        user_accounts = UserAccount.objects\
            .filter(company=self.request.user_account.company)\
            .exclude(account=self.request.user)\
            .exclude(account__is_active=False)

        if self.request.user_account.role == AccountRole.SUPERVISOR:
            return User.objects.filter(id__in=[user_account.account.id for user_account in user_accounts])

        if self.request.user_account.role == AccountRole.HR:
            return User.objects.filter(id__in=[user_account.account.id for user_account in user_accounts
                                               if not user_account.role == AccountRole.TECH_EXPERT])

        if self.request.user_account.role == AccountRole.TECH_MANAGER:
            return User.objects.filter(id__in=[user_account.account.id for user_account in user_accounts
                                               if user_account.role == AccountRole.TECH_EXPERT])

        raise Http404('Вы не можете получить список участников.')


class MemberCreationViewSet(ViewSet, CreateAPIView):
    permission_classes = [SupervisorPermission | TechManagerPermission]
    serializer_class = MemberCreateSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        user = serializer.save()
        user_registered.send(
            sender=self.__class__, user=user, request=self.request
        )

        context = {"user": user}
        to = [user.email]
        if settings.SEND_ACTIVATION_EMAIL:
            settings.EMAIL.activation(self.request, context).send(to)


class CandidatesViewSet(ApplicationViewSet):
    permission_classes = [AllowAny]

    def perform_create(self, serializer):
        user = serializer.save()
        user.is_active = True
        user.save()

    def get_serializer_class(self):
        if self.action == 'create':
            return UserCreatePasswordRetypeSerializer
        return CandidateSerializer

    def get_queryset(self):
        if self.action == 'create':
            return []

        if not self.request.user_account:
            raise Http404('Вы не можете получить список кандидатов.')

        answers = Answer.objects.filter(question__worksheet__id=self.parse_int(self.params.get('worksheet')))

        if self.request.user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT):
            answers = answers.filter(question__is_tech=True, question_author=self.request.user_account)

        return User.objects.filter(id__in=[answer.candidate.id for answer in answers])
