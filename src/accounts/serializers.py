from djoser.serializers import UserCreatePasswordRetypeSerializer
from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError

from .models import UserAccount, CompanyAccount, AccountRole
from worksheets.models import Answer
from worksheets.serializers import AnswerSerializer

User = get_user_model()


class CompanyAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyAccount
        fields = '__all__'


class UserAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccount
        exclude = ('account', )


class UserCreateSerializer(UserCreatePasswordRetypeSerializer):
    def validate(self, attrs):
        super().validate(attrs)
        data = self.context['request'].data

        company_data = {
            'name': data.get('name', None),
            'description': data.get('description', None),
            'username': data.get('username', None)
        }
        company = CompanyAccountSerializer(data=company_data)

        if not company.is_valid():
            raise ValidationError(company.errors)
        else:
            company.save()

        return attrs

    def create(self, validated_data):
        user = super().create(validated_data)

        company = CompanyAccount.objects.get(username=self.context['request'].data.get('username'))

        user_account = UserAccount(account=user, company=company, role=AccountRole.SUPERVISOR)
        user_account.save()

        return user


class UserCustomSerializer(serializers.ModelSerializer):
    company = serializers.SerializerMethodField()
    role = serializers.SerializerMethodField()  # TODO some google to make it better

    def update(self, instance, validated_data):
        data = self.context['request'].data

        company_data = {
            'position': data.get('position'),
        }

        self.context['request'].user_account.position = company_data.get('position')
        self.context['request'].user_account.save()

        return super().update(instance, validated_data)

    @staticmethod
    def get_company(obj):
        user_account = UserAccount.objects.filter(account=obj)

        if user_account.exists():
            return UserAccountSerializer(instance=user_account.first()).data
        return None

    @staticmethod
    def get_role(obj):
        user_account = UserAccount.objects.filter(account=obj)

        if user_account.exists():
            return user_account.first().role
        return None

    class Meta:
        model = User
        exclude = ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions', 'password')
        read_only_fields = ('email',)


class MemberCreateSerializer(UserCreatePasswordRetypeSerializer):
    def validate(self, attrs):
        super().validate(attrs)
        request = self.context['request']
        data = request.data
        role = data.get('role', None)

        user_account_data = {
            'role': role,
            'company': request.user_account.company.id
        }

        user_account = UserAccountSerializer(data=user_account_data)

        if not user_account.is_valid():
            raise ValidationError(user_account.errors)

        if request.user_account.role == AccountRole.TECH_MANAGER and \
                not role == AccountRole.TECH_EXPERT:
            raise ValidationError('Вы можете добавлять только Технических специалистов.')

        if request.user_account.role == AccountRole.SUPERVISOR and \
                role == AccountRole.TECH_EXPERT:
            raise ValidationError('Вы не можете добавлять Технических специалистов.')

        return attrs

    def create(self, validated_data):
        user = super().create(validated_data)

        user_account = UserAccount(account=user,
                                   company=self.context['request'].user_account.company,
                                   role=self.context['request'].data.get('role'))
        user_account.save()

        return user


class CandidateSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField(read_only=True)

    def get_answers(self, obj):
        answers = Answer.objects.filter(candidate=obj)
        return AnswerSerializer(instance=answers, many=True, context=self.context).data

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'answers')
