from django.utils.deprecation import MiddlewareMixin
from .models import UserAccount


class GetUserAccount(MiddlewareMixin):
    def process_request(self, request):
        user_account_id = request.GET.get('USER_ACCOUNT', None)
        if user_account_id:
            user_account = UserAccount.objects.get(id=int(user_account_id))
            request.user_account = user_account
        else:
            request.user_account = None
        return None
