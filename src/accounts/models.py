from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('Почта - обязательное поле.')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    avatar = models.ImageField(upload_to='avatars', default='avatars/blank.png')
    number = models.CharField(max_length=31, blank=True, null=True)
    bio = models.TextField(blank=True, null=True)

    contact_telegram = models.CharField(max_length=255, blank=True, null=True)
    contact_linkedin = models.CharField(max_length=255, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self, *args, **kwargs):
        return f'{self.email} ({self.id})'


class CompanyAccount(models.Model):
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=31, unique=True)
    description = models.TextField(blank=True, null=True)

    @property
    def owner(self):
        user = UserAccount.objects.filter(role=AccountRole.SUPERVISOR, company=self)

        if user.exists() and user.count() == 1:
            return user.first()
        raise ValueError('У аккаунта компании должен быть один руководитель.')

    def __str__(self):
        return f'{self.name} ({self.id})'


class AccountRole(models.TextChoices):
    SUPERVISOR = 'Supervisor', 'Руководитель'
    HR = 'HR', 'HR-специалист'
    TECH_MANAGER = 'Technical manager', 'Технический руководитель'
    TECH_EXPERT = 'Technical expert', 'Технический специалист'


class UserAccount(models.Model):
    account = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.ForeignKey(CompanyAccount, on_delete=models.CASCADE)

    role = models.CharField(max_length=31, choices=AccountRole.choices)
    position = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.account} - {self.role} | ({self.id})'
