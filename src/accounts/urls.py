from django.urls import path, include
from rest_framework_nested import routers
from .views import ActivationRedirect, MemberListingViewSet, MemberCreationViewSet, CandidatesViewSet


router = routers.DefaultRouter()
router.register('members', MemberListingViewSet, 'Members')
router.register('member', MemberCreationViewSet, 'Member')
router.register('candidates', CandidatesViewSet, 'Candidates')

urlpatterns = [
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('auth/', include('djoser.social.urls')),

    path('activate/<str:uid>/<str:token>/', ActivationRedirect.as_view(), name='activation'),

    path('', include(router.urls)),
]
