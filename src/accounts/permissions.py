from rest_framework.permissions import IsAuthenticated, SAFE_METHODS
from .models import AccountRole


class CompanyPermission(IsAuthenticated):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.user_account and
            request.user_account.account == request.user
        )


class SupervisorPermission(CompanyPermission):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.user_account.role == AccountRole.SUPERVISOR
        )


class HRPermission(CompanyPermission):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.user_account.role in (AccountRole.SUPERVISOR, AccountRole.HR)
        )


class TechManagerPermission(CompanyPermission):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.user_account.role == AccountRole.TECH_MANAGER
        )


class TechPermission(CompanyPermission):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.user_account.role in (AccountRole.TECH_MANAGER, AccountRole.TECH_EXPERT)
        )


class ReadOnlyPermission(CompanyPermission):
    def has_permission(self, request, view):
        return (
            super().has_permission(request, view) and
            request.method in SAFE_METHODS
        )
