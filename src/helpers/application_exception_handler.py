from rest_framework import status, serializers
from rest_framework.views import exception_handler
from rest_framework.response import Response


class ProtectedSerializer(serializers.Serializer):
    class_name = serializers.SerializerMethodField()
    pk = serializers.CharField()


def handle(exc, context):
    response = exception_handler(exc, context)
    if exc.__class__.__name__ == 'ProtectedError':
        return Response(ProtectedSerializer(exc.protected_objects, many=True).data, status=status.HTTP_409_CONFLICT)
    return response
