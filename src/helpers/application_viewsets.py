from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated


class AuthorizedViewSet(viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated]
    pagination_class = None

    @property
    def params(self):
        return self.request.query_params

    @staticmethod
    def parse_int(value):
        try:
            return int(value)
        except ValueError:
            return None


class ApplicationReadOnlyViewSet(viewsets.ReadOnlyModelViewSet, AuthorizedViewSet):
    pass


class ApplicationViewSet(viewsets.ModelViewSet, AuthorizedViewSet):
    pass
