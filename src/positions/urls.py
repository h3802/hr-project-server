from django.urls import path, include
from rest_framework_nested import routers
from .views import PositionViewSet


router_positions = routers.DefaultRouter()
router_positions.register('positions', PositionViewSet, "Position")

urlpatterns = [
    path('', include(router_positions.urls))
]
