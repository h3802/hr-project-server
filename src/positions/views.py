from helpers.application_viewsets import ApplicationViewSet
from accounts.permissions import ReadOnlyPermission, SupervisorPermission
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from .serializers import PositionSerializer
from .models import Position, PositionStatus
from accounts.models import AccountRole


class PositionViewSet(ApplicationViewSet):
    serializer_class = PositionSerializer
    permission_classes = [SupervisorPermission | ReadOnlyPermission]

    def get_queryset(self):
        queryset = Position.objects.filter(company=self.request.user_account.company)
        if not self.request.user_account.role == AccountRole.SUPERVISOR:
            queryset = queryset.filter(status=PositionStatus.OPENED)
        return queryset

    @action(detail=True, methods=['put', 'patch'])
    def open(self, request, *args, **kwargs):
        position = self.get_object()
        position.status = PositionStatus.OPENED
        position.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['put', 'patch'])
    def close(self, request, *args, **kwargs):
        position = self.get_object()
        position.status = PositionStatus.CLOSED
        position.save()

        return Response(status=status.HTTP_204_NO_CONTENT)
