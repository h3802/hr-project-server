from django.db import models


class PositionPriority(models.TextChoices):
    HIGH = 'High', 'Высокий'
    MEDIUM = 'Medium', 'Средний'
    LOW = 'Low', 'Низкий'
    DEFAULT = 'Default', 'Нет приоритета'


class PositionStatus(models.TextChoices):
    ACTIVE = 'Active', 'Активный'
    OPENED = 'Opened', 'Открытый'
    CLOSED = 'Closed', 'Закрытый'


class Position(models.Model):
    company = models.ForeignKey('accounts.CompanyAccount', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    skill = models.CharField(max_length=255)
    type_of_employment = models.CharField(max_length=255)
    responsibilities = models.TextField()
    requirements = models.TextField()
    terms = models.TextField(blank=True, null=True)
    priority = models.CharField(max_length=31, choices=PositionPriority.choices, default=PositionPriority.DEFAULT)
    status = models.CharField(max_length=31, choices=PositionStatus.choices, default=PositionStatus.ACTIVE)
    created_date = models.DateField(auto_now=True, auto_created=True)

    def __str__(self):
        return self.title
