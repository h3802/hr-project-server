from rest_framework import serializers
from .models import Position, PositionStatus
from worksheets.models import Worksheet, Question
from worksheets.serializers import WorksheetSerializer
from accounts.models import AccountRole


class PositionSerializer(serializers.ModelSerializer):
    worksheets = serializers.SerializerMethodField()

    def get_worksheets(self, obj):
        request = self.context['request']

        if request.user_account.role == AccountRole.SUPERVISOR:
            worksheets = Worksheet.objects.filter(position=obj)
        elif request.user_account.role == AccountRole.HR:
            worksheets = Worksheet.objects.filter(author=request.user_account)
        else:
            tech_questions = Question.objects.filter(worksheet__position=obj, is_tech=True, filled=False)
            worksheets = Worksheet.objects.filter(id__in=[tech_question.worksheet.id for tech_question in tech_questions])

        return WorksheetSerializer(instance=worksheets, many=True, context=self.context).data

    class Meta:
        model = Position
        fields = "__all__"
