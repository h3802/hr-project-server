from django.db import models


class Worksheet(models.Model):
    position = models.ForeignKey('positions.Position', on_delete=models.CASCADE)
    author = models.ForeignKey('accounts.UserAccount', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    opened = models.BooleanField(default=False)
    created_date = models.DateField(auto_now=True, auto_created=True)

    def __str__(self):
        return f'{self.title} ({self.id})'


class Question(models.Model):
    worksheet = models.ForeignKey(Worksheet, on_delete=models.CASCADE)
    author = models.ForeignKey('accounts.UserAccount', on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    is_tech = models.BooleanField(default=False)
    filled = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.title} ({self.id})'


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    candidate = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    text = models.TextField()
    is_correct = models.BooleanField(blank=True, null=True)

    def __str__(self):
        return f'{self.text} ({self.id})'
