# Generated by Django 3.2.13 on 2022-05-17 22:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_remove_useraccount_positions'),
        ('worksheets', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='worksheet',
            name='author',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='accounts.useraccount'),
            preserve_default=False,
        ),
    ]
